from functools import lru_cache

import config
import database


def get_session():
    session = database.Session()
    try:
        yield session
    finally:
        session.close()


# Возвращает уже существующий экземпляр вместо создания нового
@lru_cache
def get_db_settings() -> config.DBSettings:
    return config.DBSettings()

