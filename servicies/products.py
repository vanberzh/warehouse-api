from fastapi import (
    Depends,
    HTTPException,
    status,
)
from sqlalchemy.orm import Session
from typing import (
    List,
)
from models.products import Product as ProductTable
from schemas.products import (
    ProductCreate as ProductCreateModel,
    ProductUpdate as ProductUpdateModel
)

from dependencies import get_session


class ProductsService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def get_all(self, user_id: int) -> List[ProductTable]:
        products = (
            self.session
            .query(ProductTable)
            .filter(ProductTable.user_id == user_id)
            .order_by(
                ProductTable.category.desc(),
                ProductTable.name.desc(),
            )
            .all
        )
        return products

    def get_category(self, category: str) -> List[ProductTable]:
        products = (
            self.session
            .query(ProductTable)
            .filter(ProductTable.category == category)
            .order_by(
                ProductTable.name.desc(),
            )
            .all
        )
        return products

    def get(self, user_id: int, product_id: int) -> ProductTable:
        product = self._get(user_id, product_id)
        return product

    def _get(self, user_id: int, product_id: int):
        product = (
            self.session
            .query(ProductTable)
            .filter(
                ProductTable.user_id == user_id,
                ProductTable.id == product_id,
            )
            .first()
        )
        if not product:
            raise HTTPException(status.HTTP_404_NOT_FOUND)
        return product

    def create(
            self,
            user_id: int,
            product_data: ProductCreateModel
    ) -> ProductTable:
        product = ProductTable(
            **product_data.dict(),
            user_id=user_id,
        )
        self.session.add(product)
        self.session.commit()
        return product

    def update(
            self,
            user_id: int,
            product_id: int,
            product_data: ProductUpdateModel,
    ) -> ProductTable:
        product = self._get(user_id, product_id)
        for field, value in product_data:
            setattr(product, field, value)
        self.session.commit()
        return product

    def delete(
            self,
            user_id: int,
            product_id: int,
    ):
        product = self._get(user_id, product_id)
        self.session.delete(product)
        self.session.commit()
