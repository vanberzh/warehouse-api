from fastapi import (
    Depends,
    HTTPException,
    status
)
from fastapi.security import OAuth2PasswordBearer
from jose import (
    jwt,
    JWTError
)
from passlib.hash import bcrypt
from pydantic import ValidationError
from sqlalchemy.orm import Session
from datetime import (
    datetime,
    timedelta,
)
from models.users import User as UserTable
from schemas.users import (
    User as UserModel,
    UserCreate as UserCreateModel,
    Token as TokenModel,
)

from dependencies import get_session
from config import settings_jwt

oauth2_scheme = OAuth2PasswordBearer(tokenUrl='/auth/sign-in/')


def get_current_user(token: str = Depends(oauth2_scheme)) -> UserModel:
    return AuthService.verify_token(token)


class AuthService:
    @classmethod
    def verify_password(cls, plain_password: str, hashed_password: str) -> bool:
        return bcrypt.verify(plain_password, hashed_password)

    @classmethod
    def hash_password(cls, password: str) -> str:
        return bcrypt.hash(password)

    @classmethod
    def verify_token(cls, token: str) -> UserModel:
        exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Could not validate credentials',
            headers={'WWW-Authenticate': 'Bearer'},
        )
        try:
            payload = jwt.decode(
                token,
                settings_jwt.secret_key,
                algorithms=[settings_jwt.algorithm],
            )
        except JWTError:
            raise exception from None

        user_data = payload.get('user')

        try:
            user = UserModel.parse_obj(user_data)
        except ValidationError:
            raise exception from None

        return user

    @classmethod
    def create_token(cls, user: UserTable) -> TokenModel:
        user_data = UserModel.from_orm(user)
        now = datetime.utcnow()
        payload = {
            'iat': now,
            'nbf': now,
            'exp': now + timedelta(seconds=settings_jwt.expires_s),
            'sub': str(user_data.id),
            'user': user_data.dict(),
        }
        token = jwt.encode(
            payload,
            settings_jwt.secret_key,
            algorithm=settings_jwt.algorithm,
        )
        return TokenModel(access_token=token)

    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def register_new_user(
            self,
            user_data: UserCreateModel,
    ) -> TokenModel:
        user = UserTable(
            email=user_data.email,
            username=user_data.username,
            first_name=user_data.first_name,
            last_name=user_data.last_name,
            is_seller=user_data.is_seller,
            created_at=user_data.created_at,
            updated_at=user_data.updated_at,
            hashed_password=self.hash_password(user_data.password)
        )
        self.session.add(user)
        self.session.commit()
        return self.create_token(user)

    def authenticate_user(
            self,
            username: str,
            password: str,
    ) -> TokenModel:
        exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='Incorrect username or password',
            headers={'WWW-Authenticate': 'Bearer'}
        )

        user = (
            self.session
            .query(UserTable)
            .filter(UserTable.username == username)
            .first()
        )

        if not user:
            raise exception

        if not self.verify_password(password, user.hashed_password):
            raise exception

        return self.create_token(user)

