from fastapi import (
    Depends,
    HTTPException,
    status,
)
from sqlalchemy.orm import Session
from typing import List

from models.orders import Order as OrderTable
from schemas.orders import (
    OrderCreate as OrderCreateModel,
    OrderUpdate as OrderUpdateModel
)

from dependencies import get_session


class OrdersService:
    def __init__(self, session: Session = Depends(get_session)):
        self.session = session

    def get_all(self, user_id: int) -> List[OrderTable]:
        orders = (
            self.session
            .query(OrderTable)
            .filter(OrderTable.user_id == user_id)
            .order_by(
                OrderTable.category.desc(),
                OrderTable.name.desc(),
            )
            .all
        )
        return orders

    def get(self, user_id: int, order_id: int) -> OrderTable:
        order = self._get(user_id, order_id)
        return order

    def _get(self, user_id: int, order_id: int):
        order = (
            self.session
            .query(OrderTable)
            .filter(
                OrderTable.user_id == user_id,
                OrderTable.id == order_id,
            )
            .first()
        )
        if not order:
            raise HTTPException(status.HTTP_404_NOT_FOUND)
        return order

    def create(
            self,
            user_id: int,
            order_data: OrderCreateModel
    ) -> OrderTable:
        order = OrderTable(
            **order_data.dict(),
            user_id=user_id,
        )
        self.session.add(order)
        self.session.commit()
        return order

    def update(
            self,
            user_id: int,
            order_id: int,
            order_data: OrderUpdateModel,
    ) -> OrderTable:
        order = self._get(user_id, order_id)
        for field, value in order_data:
            setattr(order, field, value)
        self.session.commit()
        return order

    def delete(
            self,
            user_id: int,
            order_id: int,
    ):
        order = self._get(user_id, order_id)
        self.session.delete(order)
        self.session.commit()
