from pydantic import BaseModel
import datetime


class BaseOrder(BaseModel):
    quantity: int
    product_id: int
    price: float
    quantity: str
    user_id: int
    updated_at: datetime.datetime


class OrderCreate(BaseOrder):
    pass


class OrderUpdate(BaseOrder):
    pass


class Order(BaseOrder):
    id: int
    created_at: datetime.datetime

    class Config:
        orm_mode = True

