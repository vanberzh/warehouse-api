from pydantic import (
    BaseModel,
    EmailStr,
    constr
)
import datetime


class BaseUser(BaseModel):
    email: EmailStr
    username: str
    first_name: str
    last_name: str
    is_seller: bool


class UserCreate(BaseUser):
    password: constr(min_length=8)
    created_at: datetime.datetime
    updated_at: datetime.datetime


class User(BaseUser):
    id: int

    class Config:
        orm_mode = True


class Token(BaseModel):
    access_token: str
    token_type: str = 'bearer'

