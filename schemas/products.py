from pydantic import BaseModel
import datetime


class BaseProduct(BaseModel):
    category: str
    name: str
    price: float
    quantity: str
    user_id: int
    updated_at: datetime.datetime


class ProductCreate(BaseProduct):
    pass


class ProductUpdate(BaseProduct):
    pass


class Product(BaseProduct):
    id: int
    created_at: datetime.datetime

    class Config:
        orm_mode = True

