from pydantic import BaseSettings


class DBSettings(BaseSettings):
    username: str
    password: str
    database: str
    host: str
    port: str

    class Config:
        env_prefix = "DB_"
        env_file = ".env"


class JWTSettings(BaseSettings):
    secret_key: str
    algorithm: str = 'HS256'
    expires_s: int = 3600

    class Config:
        env_prefix = "JWT_"
        env_file = ".env"


settings_jwt = JWTSettings()

