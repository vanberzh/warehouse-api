from sqlalchemy import (
    Column,
    Integer,
    String,
    Boolean,
    DateTime,
)
import datetime
from database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, autoincrement=True, unique=True)
    first_name = Column(String)
    last_name = Column(String)
    username = Column(String)
    email = Column(String, primary_key=True, unique=True)
    hashed_password = Column(String)
    is_seller = Column(Boolean)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)

