from fastapi import (
    APIRouter,
    Depends,
    status,
)
from fastapi.security import OAuth2PasswordRequestForm

from schemas.users import (
    Token as TokenModel,
    User as UserModel,
    UserCreate as UserCreateModel,
)
from servicies.auth import (
    AuthService,
    get_current_user,
)


router = APIRouter(
    prefix='/auth',
    tags=['auth'],
)


@router.post(
    '/sign_up',
    response_model=TokenModel,
    status_code=status.HTTP_201_CREATED,
)
def sign_up(
        user_data: UserCreateModel,
        auth_service: AuthService = Depends(),
):
    return auth_service.register_new_user(user_data)


@router.post(
    '/sign_in',
    response_model=TokenModel,
)
def sign_in(
        auth_data: OAuth2PasswordRequestForm = Depends(),
        auth_service: AuthService = Depends(),
):
    return auth_service.authenticate_user(
        auth_data.username,
        auth_data.password,
    )


@router.get(
    '/user/',
    response_model=UserModel,
)
def get_user(user: UserModel = Depends(get_current_user)):
    return user

