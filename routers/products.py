from fastapi import (
    APIRouter,
    Depends,
    status,
    Response,
)

from typing import List

from schemas.users import User as UserModel
from schemas.products import (
    Product as ProductModel,
    ProductCreate as ProductCreateModel,
    ProductUpdate as ProductUpdateModel,
)
from servicies.auth import get_current_user
from servicies.products import ProductsService


router = APIRouter(
    prefix='/products',
    tags=['products'],
)


@router.get(
    '/',
    response_model=List[ProductModel],
)
def get_products(
        user: UserModel = Depends(get_current_user),
        products_service: ProductsService = Depends(),
):
    return products_service.get_all(user.id)


@router.post(
    '/',
    response_model=ProductModel,
    status_code=status.HTTP_201_CREATED,
)
def create_order(
        product_data: ProductCreateModel,
        user: UserModel = Depends(get_current_user),
        products_service: ProductsService = Depends(),
):
    return products_service.create(
        user.id,
        product_data,
    )


@router.get(
    '/{product_id}',
    response_model=ProductModel,
)
def get_order(
        product_id: int,
        user: UserModel = Depends(get_current_user),
        products_service: ProductsService = Depends(),
):
    return products_service.get(
        user.id,
        product_id
    )


@router.put(
    '/{product_id}',
    response_model=ProductModel,
)
def update_order(
        product_id: int,
        product_data: ProductUpdateModel,
        user: UserModel = Depends(get_current_user),
        products_service: ProductsService = Depends(),
):
    return products_service.update(
        user.id,
        product_id,
        product_data,
    )


@router.delete(
    '/{product_id}',
    status_code=status.HTTP_204_NO_CONTENT,
)
def delete_order(
        product_id: int,
        user: UserModel = Depends(get_current_user),
        products_service: ProductsService = Depends(),
):
    products_service.delete(
        user.id,
        product_id,
    )
    return Response(status_code=status.HTTP_204_NO_CONTENT)

