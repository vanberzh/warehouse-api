from fastapi import (
    APIRouter,
    Depends,
    status,
    Response,
)

from typing import List

from schemas.users import User as UserModel
from schemas.orders import (
    Order as OrderModel,
    OrderCreate as OrderCreateModel,
    OrderUpdate as OrderUpdateModel,
)
from servicies.auth import get_current_user
from servicies.orders import OrdersService


router = APIRouter(
    prefix='/orders',
    tags=['orders'],
)


@router.get(
    '/',
    response_model=List[OrderModel],
)
def get_orders(
        user: UserModel = Depends(get_current_user),
        orders_service: OrdersService = Depends(),
):
    return orders_service.get_all(user.id)


@router.post(
    '/',
    response_model=OrderModel,
    status_code=status.HTTP_201_CREATED,
)
def create_order(
        order_data: OrderCreateModel,
        user: UserModel = Depends(get_current_user),
        orders_service: OrdersService = Depends(),
):
    return orders_service.create(
        user.id,
        order_data,
    )


@router.get(
    '/{order_id}',
    response_model=OrderModel,
)
def get_order(
        order_id: int,
        user: UserModel = Depends(get_current_user),
        orders_service: OrdersService = Depends(),
):
    return orders_service.get(
        user.id,
        order_id
    )


@router.put(
    '/{order_id}',
    response_model=OrderModel,
)
def update_order(
        order_id: int,
        order_data: OrderUpdateModel,
        user: UserModel = Depends(get_current_user),
        orders_service: OrdersService = Depends(),
):
    return orders_service.update(
        user.id,
        order_id,
        order_data,
    )


@router.delete(
    '/{order_id}',
    status_code=status.HTTP_204_NO_CONTENT,
)
def delete_order(
        order_id: int,
        user: UserModel = Depends(get_current_user),
        orders_service: OrdersService = Depends(),
):
    orders_service.delete(
        user.id,
        order_id,
    )
    return Response(status_code=status.HTTP_204_NO_CONTENT)

