import uvicorn
from fastapi import FastAPI
from database import Base, engine


from routers import (
    auth,
    products,
    orders,
)

Base.metadata.create_all(bind=engine)

app = FastAPI()


app.include_router(auth.router)
app.include_router(orders.router)
app.include_router(products.router)


if __name__ == "__main__":
    uvicorn.run("main:app", port=8000, host="127.0.0.1", reload=True)
