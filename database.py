from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


from dependencies import get_db_settings


settings = get_db_settings()

DATABASE_URL = f"postgresql://{settings.username}:" \
               f"{settings.password}@{settings.host}:" \
               f"{settings.port}/{settings.database}"

engine = create_engine(DATABASE_URL)

Session = sessionmaker(
    autocommit=False,
    autoflush=False,
    bind=engine
)

Base = declarative_base()

